<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Tarea extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'titulo',
        'descripcion',
        'status',
        'asignable_id',
        'asignable_type'
    ];

    public function asignable() {
        return $this->morphTo();
    }

    public function setCompleted() {
        $this->status = 'completed';
        $this->save();
        return true;
    }

    public function scopeOpen($query) {
        $query->where('status', 'open');
    }
}
