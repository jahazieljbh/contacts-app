<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NegocioCategoria extends Model
{
    use HasFactory, SoftDeletes;

    public function negocio() {
        return $this->belongsToMany(Negocio::class, 'categoria_tiene_negocio');
    }
}
