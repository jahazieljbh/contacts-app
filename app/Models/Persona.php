<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Persona extends Model
{
    use HasFactory, SoftDeletes;

    protected $with = ['negocio'];

    public function negocio() {
        return $this->belongsTo(Negocio::class)->withTrashed();
    }

    public function tareas() {
        return $this->morphMany(Tarea::class, 'asignable');
    }

    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
