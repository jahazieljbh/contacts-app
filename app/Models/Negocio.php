<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Negocio extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
      'negocio_nombre',
      'negocio_correo',
      'negocio_telefono'
    ];

    public function personas() {
        return $this->hasMany(Persona::class);
    }

    public function categorias() {
        return $this->belongsToMany(NegocioCategoria::class, 'categoria_tiene_negocio');
    }

    public function tareas() {
        return $this->morphMany(Tarea::class, 'asignable');
    }

    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
