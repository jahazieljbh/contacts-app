<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Tag extends Model
{
    use HasFactory, SoftDeletes;

    public function personas() {
        return $this->morphedByMany(Persona::class, 'taggable');
    }

    public function negocios() {
        return $this->morphedByMany(Negocio::class, 'taggable');
    }
}
