<?php

namespace App\Console\Commands;

use App\Models\Persona;
use Illuminate\Console\Command;

class CheckForFechadenacimiento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cwb:birthdayCheck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commando de fecha de nacimiento o cumpleaños';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $persona = Persona::whereMonth('fecha_de_nacimiento', '=', now()->addDays(5)->format('m'))
        ->whereDay('fecha_de_nacimiento', '=', now()->addDays(5)->format('d'))
        ->pluck('persona_nombre');
        dd($persona);
    }
}
