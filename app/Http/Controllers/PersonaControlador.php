<?php

namespace App\Http\Controllers;

use App\Models\Negocio;
use App\Models\Persona;
use App\Models\Tag;
use Illuminate\Http\Request;

class PersonaControlador extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('persona.index')->with('personas', Persona::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('persona.create')->with(['negocios' => Negocio::all(), 'tags' => Tag::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'persona_nombre' => 'required',
            'persona_apellido' => 'required',
            'persona_correo' => 'nullable|email',
        ]);

        $persona = new Persona();
        $persona->persona_nombre = $request->input('persona_nombre');
        $persona->persona_apellido = $request->input('persona_apellido');
        $persona->persona_correo = $request->input('persona_correo');
        $persona->persona_telefono = $request->input('persona_telefono');
        $persona->negocio_id = $request->input('negocio_id');
        $persona->save();

        return redirect(route('persona.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Persona $persona)
    {
        return view('persona.detail')->with('persona', $persona);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Persona $persona)
    {
        return view('persona.edit')->with(['persona' => $persona, 'negocios' => Negocio::all(), 'tags' => Tag::all()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Persona $persona)
    {
        $validated = $request->validate([
            'persona_nombre' => 'required',
            'persona_apellido' => 'required',
            'persona_correo' => 'nullable|email',
        ]);

        $persona->persona_nombre = $request->input('persona_nombre');
        $persona->persona_apellido = $request->input('persona_apellido');
        $persona->persona_correo = $request->input('persona_correo');
        $persona->persona_telefono = $request->input('persona_telefono');
        $persona->negocio_id = $request->input('negocio_id');
        $persona->save();

        $persona->tags()->sync($request->input('tags'));

        return redirect(route('persona.index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Persona $persona)
    {
        $persona->delete();

        return redirect(route('persona.index'));
    }
}
