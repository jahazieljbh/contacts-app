<?php

namespace App\Http\Controllers;

use App\Models\Negocio;
use App\Models\Persona;
use App\Models\Tarea;
use Illuminate\Http\Request;

class TareaControlador extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('tarea.index')->with('tareas', Tarea::open()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'descripcion' => 'required'
        ]);

        $targetModel = match($request->input('target_model')) {
            'negocio' => Negocio::find($request->input('asignable_id')),
            'persona' => Persona::find($request->input('asignable_id'))
        };

        $targetModel->tareas()->create([
            'titulo' => $request->input('titulo'),
            'descripcion' => $request->input('descripcion')
        ]);

        return redirect()->back();
    }

    /**
     * Update the specified resource status in storage.
     */
    public function complete(Request $request, Tarea $tarea)
    {
        $tarea->setCompleted();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Tarea $tarea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tarea $tarea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Tarea $tarea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tarea $tarea)
    {
        //
    }
}
