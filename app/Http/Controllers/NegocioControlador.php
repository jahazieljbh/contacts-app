<?php

namespace App\Http\Controllers;

use App\Http\Requests\NegocioRequest;
use App\Models\Negocio;
use App\Models\Tag;
use Illuminate\Http\Request;

class NegocioControlador extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('negocio.index')->with(['negocios' => Negocio::withCount('personas')->paginate(10), 'tags' => Tag::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('negocio.create')->with(['tags' => Tag::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NegocioRequest $request)
    {
        $negocio = Negocio::create(($request->validated()));

        return redirect(route('negocio.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Negocio $negocio)
    {
        return view('negocio.detail')->with('negocio', $negocio);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Negocio $negocio)
    {
        return view('negocio.edit')->with(['negocio' => $negocio, 'tags' => Tag::all()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NegocioRequest $request, Negocio $negocio)
    {
        $negocio->update($request->validated());

        $negocio->tags()->sync($request->input('tags'));

        return redirect(route('negocio.index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Negocio $negocio)
    {
        $negocio->delete();

        return redirect(route('negocio.index'));
    }
}
