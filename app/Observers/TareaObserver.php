<?php

namespace App\Observers;

use App\Mail\TareaCreated;
use App\Models\Tarea;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class TareaObserver
{
    /**
     * Handle the Tarea "created" event.
     */
    public function created(Tarea $tarea): void
    {
        Mail::to(User::find(1))->send(new TareaCreated());
    }

    /**
     * Handle the Tarea "updated" event.
     */
    public function updated(Tarea $tarea): void
    {
        //
    }

    /**
     * Handle the Tarea "deleted" event.
     */
    public function deleted(Tarea $tarea): void
    {
        //
    }

    /**
     * Handle the Tarea "restored" event.
     */
    public function restored(Tarea $tarea): void
    {
        //
    }

    /**
     * Handle the Tarea "force deleted" event.
     */
    public function forceDeleted(Tarea $tarea): void
    {
        //
    }
}
