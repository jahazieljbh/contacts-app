<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('negocio_categorias', function (Blueprint $table) {
            $table->id();
            $table->string('categoria_nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('categoria_tiene_negocio', function (Blueprint $table) {
            $table->id();
            $table->integer('negocio_categoria_id');
            $table->integer('negocio_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('negocio_categorias');
    }
};
