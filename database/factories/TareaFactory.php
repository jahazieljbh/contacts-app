<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tarea>
 */
class TareaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titulo' => fake()->sentence(),
            'descripcion' => fake()->sentence(),
            'status' => fake()->randomElement(['open', 'completed']),
            'asignable_id' => fake()->numberBetween(1, 70),
            'asignable_type' => fake()->randomElement(['App\Models\Negocio','App\Models\Persona'])
        ];
    }
}
