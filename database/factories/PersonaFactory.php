<?php

namespace Database\Factories;

use App\Models\Negocio;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Persona>
 */
class PersonaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $negocios = Negocio::pluck('id');
        return [
            'persona_nombre' => fake()->firstName(),
            'persona_apellido' => fake()->lastName(),
            'persona_correo' => fake()->unique()->safeEmail(),
            'fecha_de_nacimiento' => fake()->date('Y-m-d'),
            'persona_telefono' => fake()->phoneNumber(),
            'negocio_id' => (fake()->boolean(50) ? fake()->randomElement($negocios) : NULL)
        ];
    }
}
