<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Negocio>
 */
class NegocioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'negocio_nombre' => fake()->company(),
            'negocio_correo' => fake()->unique()->companyEmail(),
            'negocio_telefono' => fake()->phoneNumber(),
        ];
    }
}
