<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Personas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flex items-center justify-end">
                        <a class="bg-blue-600 text-white  py-2 px-3 rounded-full" href="{{route('persona.create')}}">Crear Persona</a>
                    </div>
                    <table class="table-fixed border-separate border-spacing-6 text-sm">
                        <thead>
                            <tr>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Nombre</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Correo</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Teléfono</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Negocio</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Etiquetas</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($personas as $persona)
                            <tr>
                                <td>
                                    <a href="{{route('persona.show', $persona)}}">
                                        {{ $persona->persona_nombre }} {{ $persona->persona_apellido }}
                                    </a>
                                </td>
                                <td>{{ $persona->persona_correo }}</td>
                                <td>{{ $persona->persona_telefono }}</td>
                                <td class="{{($persona->negocio?->deleted_at)?'line-through':''}}">
                                    {{ $persona->negocio?->negocio_nombre?:'S/N' }}
                                </td>
                                <td>
                                    @foreach($persona->tags as $tag)
                                        <span class="text-xs inline-flex items-center font-bold leading-sm uppercase px-3 py-1 bg-blue-200 text-blue-700 rounded-full my-1">{{$tag->tag_name}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{route('persona.edit', $persona->id)}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$personas->links()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
