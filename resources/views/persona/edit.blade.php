<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Personas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h3 class="font-semibold pb-5">Editar Persona</h3>
                    <form action="{{route('persona.update', $persona->id)}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="grid grid-cols-1 sm:grid-cols-6 gap-6">
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_nombre">Nombre</label>
                                <input class="block w-full rounded-md" type="text" name="persona_nombre" id="persona_nombre" value="{{old('persona_nombre', $persona->persona_nombre)}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_apellido">Apellido</label>
                                <input class="block w-full rounded-md" type="text" name="persona_apellido" id="persona_apellido" value="{{old('persona_apellido', $persona->persona_apellido, $persona->persona_nombre)}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_correo">Correo</label>
                                <input class="block w-full rounded-md" type="text" name="persona_correo" id="persona_correo" value="{{old('persona_correo', $persona->persona_correo)}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_telefono">Telefono</label>
                                <input class="block w-full rounded-md" type="text" name="persona_telefono" id="persona_telefono" value="{{old('persona_telefono', $persona->persona_telefono)}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="negocios">Empresa</label>
                                <select class="block w-full rounded-md" name="negocio_id" id="negocio_id">
                                    <option value="" @selected("" == old('negocio_id', $persona->negocio_id))>Ninguna Empresa Seleccionada</option>
                                    @foreach($negocios as $negocio)
                                        <option value="{{ $negocio->id }}" @selected($negocio->id == old('negocio_id', $persona->negocio_id))>
                                            {{ $negocio->negocio_nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </span>
                        </div>

                        <h4 class="font-semibold pt-5">Etiquetas</h4>
                        <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                            @foreach($tags as $tag)
                                <span class="sm:col-span-2">
                                    <input type="checkbox" name="tags[]" id="tag{{$tag->id}}" value="{{$tag->id}}" @checked(in_array($tag->tag_name, $persona->tags->pluck('tag_name')->toArray()))>
                                    <label for="tag{{$tag->id}}">{{$tag->tag_name}}</label>
                                </span>
                            @endforeach
                        </div>

                        <div class="flex items-center justify-end gap-6 mt-6">
                            <a href="{{route('persona.index')}}">Cancelar</a>
                            <button class="bg-blue-600 hover:bg-blue-700 text-white py-2 px-6 rounded-full" type="submit">Editar</button>
                        </div>
                    </form>

                    <form action="{{route('persona.destroy', $persona->id)}}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="border border-red-500 text-white bg-red-500 mt-6 p-6 rounded-lg">
                            <div class="flex justify-between">
                                <h3 class="font-semibold">Eliminar esta persona</h3>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
                                </svg>

                            </div>
                            <p>Esta acción elimina a la persona {{ $persona->persona_nombre }} {{ $persona->persona_apellido }} de la base de datos. <strong class="text-black">No hay vuelta atrás.</strong></p>
                            <button type="submit" class="hover:text-black">Eliminar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
