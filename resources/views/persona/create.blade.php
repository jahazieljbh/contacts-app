<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Personas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h3 class="font-semibold pb-5">Crear Persona</h3>
                    <form action="{{route('persona.store')}}" method="POST">
                        @csrf

                        <div class="grid grid-cols-1 sm:grid-cols-6 gap-6">
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_nombre">Nombre</label>
                                <input class="block w-full rounded-md" type="text" name="persona_nombre" id="persona_nombre" value="{{old('persona_nombre')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_apellido">Apellido</label>
                                <input class="block w-full rounded-md" type="text" name="persona_apellido" id="persona_apellido" value="{{old('persona_apellido')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_correo">Correo</label>
                                <input class="block w-full rounded-md" type="text" name="persona_correo" id="persona_correo" value="{{old('persona_correo')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="persona_telefono">Telefono</label>
                                <input class="block w-full rounded-md" type="text" name="persona_telefono" id="persona_telefono" value="{{old('persona_telefono')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="negocio">Empresa</label>
                                <select class="block w-full rounded-md" name="negocio_id" id="negocio_id">
                                    <option value="" selected>Ninguna Empresa Seleccionada</option>
                                    @foreach($negocios as $negocio)
                                        <option value="{{ $negocio->id }}" @selected($negocio->id == old('negocio_id'))>
                                            {{ $negocio->negocio_nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </span>
                        </div>

                        <h4 class="font-semibold pt-5">Etiquetas</h4>
                        <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                            @foreach($tags as $tag)
                                <span class="sm:col-span-2">
                                    <input type="checkbox" name="tags[]" id="tag{{$tag->id}}" value="{{$tag->id}}">
                                    <label for="tag{{$tag->id}}">{{$tag->tag_name}}</label>
                                </span>
                            @endforeach
                        </div>

                        <div class="flex items-center justify-end gap-6 mt-6">
                            <a href="{{route('persona.index')}}">Cancelar</a>
                            <button class="bg-blue-600 text-white py-2 px-6 rounded-full" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
