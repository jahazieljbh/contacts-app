<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Personas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="grid grid-cols-1 sm:grid-cols-6 gap-6">
                        <div class="sm:col-span-3">
                            <h3 class="font-semibold text-l pb-5">Detalles</h3>
                            <dl>
                                <dt class="font-semibold">Nombre de la Persona</dt>
                                <dd class="pl-3">{{$persona->persona_nombre}} {{$persona->persona_apellido}}</dd>
                                <dt class="font-semibold">Correo Electrónico</dt>
                                <dd class="pl-3">{{$persona->persona_correo}}</dd>
                                <dt class="font-semibold">Cumpleaños</dt>
                                <dd class="pl-3">{{$persona->fecha_de_nacimiento}}</dd>
                                <dt class="font-semibold">Número de Teléfono</dt>
                                <dd class="pl-3">{{$persona->persona_telefono}}</dd>
                                <dt class="font-semibold">Negocio</dt>
                                <dd class="pl-3">{{$persona->negocio?->negocio_nombre?:'S/N'}}</dd>
                            </dl>
                            <div class="pt-3">
                                <a href="{{route('persona.edit', $persona->id)}}" class="bg-blue-500 hover:bg-blue-600 py-2 px-3 rounded-full text-white">Editar Datos</a>
                            </div>
                        </div>
                        <div class="sm:col-span-3">
                            <h3 class="text-l font-semibold pb-5">Crear Nueva Tarea</h3>
                            <form action="{{route('tarea.store')}}" method="POST">
                                @csrf
                                <input type="hidden" name="asignable_id" value="{{$persona->id}}">
                                <input type="hidden" name="target_model" value="persona">
                                <div class="grid grid-cols-1 sm:grid-cols-6 gap-6">
                                    <span class="sm:col-span-6">
                                        <label class="block" for="titulo">Titulo</label>
                                        <input class="block w-full" type="text" name="titulo" id="titulo" value="{{old('titulo')}}">
                                    </span>
                                    <span class="sm:col-span-6">
                                        <label class="block" for="descripcion">Descripcion</label>
                                        <textarea class="block w-full" name="descripcion" id="descripcion" cols="30" rows="10">{{old('descripcion')}}</textarea>
                                    </span>
                                </div>
                                <div class="mt-5 flex items-center justify-end gap-6">
                                    <button class="flex items-center justify-end ml-2 bg-blue-500 hover:bg-blue-600 text-white px-3 py-2 rounded-full" type="submit">Guardar</button>
                                </div>
                            </form>
                            <h3 class="text-l font-semibold pb-5">Tareas</h3>
                            @foreach($persona->tareas->sortByDesc('created_at') as $tarea)
                                <p class="font-semibold">{{$tarea->titulo}}</p>
                                <p>{{$tarea->descripcion}}</p>
                                @if($tarea->status == 'open')
                                    <div class="m-3">
                                        <form action="{{route('tarea.complete', $tarea->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button class="bg-blue-100 hover:bg-blue-200 text-blue-500 rounded-full py-2 px-3" type="submit">Completar</button>
                                        </form>
                                    </div>
                                @elseif($tarea->status == 'completed')
                                    <p>Completed</p>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
