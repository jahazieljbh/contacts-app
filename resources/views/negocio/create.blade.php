<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Negocio') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h3 class="font-semibold pb-5">Crear Negocio</h3>
                    <form action="{{route('negocio.store')}}" method="POST">
                        @csrf
                        @if($errors->any())
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="grid grid-cols-1 sm:grid-cols-6 gap-6">
                            <span class="sm:col-span-3">
                                <label class="block" for="negocio_nombre">Nombre</label>
                                <input class="block w-full rounded-md" type="text" name="negocio_nombre" id="negocio_nombre" value="{{old('negocio_nombre')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="negocio_correo">Correo</label>
                                <input class="block w-full rounded-md" type="text" name="negocio_correo" id="negocio_correo" value="{{old('negocio_correo')}}">
                            </span>
                            <span class="sm:col-span-3">
                                <label class="block" for="negocio_telefono">Telefono</label>
                                <input class="block w-full rounded-md" type="text" name="negocio_telefono" id="negocio_telefono" value="{{old('negocio_telefono')}}">
                            </span>
                        </div>
                        <div class="flex items-center justify-end gap-6 mt-6">
                            <a href="{{route('negocio.index')}}">Cancelar</a>
                            <button class="bg-blue-600 text-white py-2 px-6 rounded-full" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
