<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tareas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <table class="table-fixed border-separate border-spacing-6 text-sm justify-center items-center">
                        <thead>
                            <tr>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Titulo</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Para</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Estado</th>
                                <th class="border-b dark:border-slate-600 font-medium pt-0 pb-3 text-left">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tareas as $tarea)
                            <tr>
                                <td>{{ $tarea->titulo }}</td>
                                <td class="flex space-x-1">
                                    @if($tarea->asignable_type === 'App\Models\Negocio')
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                                            <path fill-rule="evenodd" d="M4.5 2.25a.75.75 0 000 1.5v16.5h-.75a.75.75 0 000 1.5h16.5a.75.75 0 000-1.5h-.75V3.75a.75.75 0 000-1.5h-15zM9 6a.75.75 0 000 1.5h1.5a.75.75 0 000-1.5H9zm-.75 3.75A.75.75 0 019 9h1.5a.75.75 0 010 1.5H9a.75.75 0 01-.75-.75zM9 12a.75.75 0 000 1.5h1.5a.75.75 0 000-1.5H9zm3.75-5.25A.75.75 0 0113.5 6H15a.75.75 0 010 1.5h-1.5a.75.75 0 01-.75-.75zM13.5 9a.75.75 0 000 1.5H15A.75.75 0 0015 9h-1.5zm-.75 3.75a.75.75 0 01.75-.75H15a.75.75 0 010 1.5h-1.5a.75.75 0 01-.75-.75zM9 19.5v-2.25a.75.75 0 01.75-.75h4.5a.75.75 0 01.75.75v2.25a.75.75 0 01-.75.75h-4.5A.75.75 0 019 19.5z" clip-rule="evenodd" />
                                        </svg>
                                        <span>
                                            {{$tarea->asignable->negocio_nombre}}
                                        </span>
                                    @elseif($tarea->asignable_type === 'App\Models\Persona')
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                                            <path fill-rule="evenodd" d="M7.5 6a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zM3.751 20.105a8.25 8.25 0 0116.498 0 .75.75 0 01-.437.695A18.683 18.683 0 0112 22.5c-2.786 0-5.433-.608-7.812-1.7a.75.75 0 01-.437-.695z" clip-rule="evenodd" />
                                        </svg>
                                        <span>
                                            {{$tarea->asignable->persona_nombre}}
                                        </span>
                                    @endif
                                </td>
                                <td>{{$tarea->status}}</td>
                                <td>
                                    @if($tarea->status)
                                        <div class="pt-3">
                                            <form action="{{route('tarea.complete', $tarea->id)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <button class="{{ $tarea->status == 'open' ? "bg-blue-200 hover:bg-blue-300 text-blue-500 rounded-full py-2 px-3" : "bg-blue-200 text-blue-500 font-bold rounded-full py-2 px-3 opacity-50 cursor-not-allowed" }}" type="submit">Completed</button>
                                            </form>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$tareas->links()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
